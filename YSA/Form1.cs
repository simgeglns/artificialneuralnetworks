﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YSA
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static int girisprosesSayisi, arakatmanprosesSayisi, cikisprosesSayisi, iterasyonSayisi;
        private void button1_Click(object sender, EventArgs e)
        {
           
            Form2 frm = new Form2();

            girisprosesSayisi = Convert.ToInt32(textBox1.Text);
            arakatmanprosesSayisi = Convert.ToInt32(textBox2.Text);
            cikisprosesSayisi = Convert.ToInt32(textBox3.Text);
            iterasyonSayisi = Convert.ToInt32(textBox4.Text);

            frm.Show();
            this.Hide();

        }
    }
}
