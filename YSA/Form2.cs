﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YSA
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        
        List<TextBox> girisProsesler = new List<TextBox>();      

        List<TextBox> girisaraAgirliklar = new List<TextBox>();
        List<TextBox> aracikisAgirliklar = new List<TextBox>();

        List<TextBox> cikisProsesler = new List<TextBox>();

        private void Form2_Load(object sender, EventArgs e)
        {
            int x = Form1.girisprosesSayisi;
            int y = Form1.arakatmanprosesSayisi;
            int z = Form1.cikisprosesSayisi;

            for (int i=0; i<x; i++)
            {
                Label label = new Label();
                label.Location = new System.Drawing.Point(26, 75 + (i * 25));
                label.Width = 30;
                label.Text = (i + 1) + ".";
                this.Controls.Add(label);
                TextBox yenideger = new TextBox();
                yenideger.Location = new System.Drawing.Point(60, 75 + (25 * i));
                yenideger.Size = new System.Drawing.Size(100, 25);
                this.Controls.Add(yenideger);
                girisProsesler.Add(yenideger);
            }

            int konum = 0;
            for (int i = 0; i < x; i++)
            {
                
                for (int t=0; t < y; t++)
                {
                    
                    Label label = new Label();
                    label.Location = new System.Drawing.Point(256, 75 + (t + konum + 2));
                    label.Width = 30;
                    label.Text = (i+1)+"."+ (t+1) +".";
                    this.Controls.Add(label);
                    TextBox yenideger = new TextBox();
                    yenideger.Location = new System.Drawing.Point(290, 75 + (t + konum));
                    yenideger.Size = new System.Drawing.Size(100, 25);
                    this.Controls.Add(yenideger);
                    girisaraAgirliklar.Add(yenideger);

                    konum = konum + 25;
                }

                konum = konum + 5;
            }

            konum = 0;
            for (int i = 0; i < y; i++)
            {

                for (int t = 0; t < z; t++)
                {

                    Label label = new Label();
                    label.Location = new System.Drawing.Point(526, 75 + (t + konum + 2));
                    label.Width = 30;
                    label.Text = (i + 1) + "." + (t + 1) + ".";
                    this.Controls.Add(label);
                    TextBox yenideger = new TextBox();
                    yenideger.Location = new System.Drawing.Point(560, 75 + (t + konum));
                    yenideger.Size = new System.Drawing.Size(100, 25);
                    this.Controls.Add(yenideger);
                    aracikisAgirliklar.Add(yenideger);

                    konum = konum + 25;
                }

                konum = konum + 5;
            }

            for (int i = 0; i < z; i++)
            {
                Label label = new Label();
                label.Location = new System.Drawing.Point(790, 75 + (i * 25));
                label.Width = 30;
                label.Text = (i + 1) + ".";
                this.Controls.Add(label);
                TextBox yenideger = new TextBox();
                yenideger.Location = new System.Drawing.Point(820, 75 + (25 * i));
                yenideger.Size = new System.Drawing.Size(100, 25);
                this.Controls.Add(yenideger);
                cikisProsesler.Add(yenideger);
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            int x = Form1.girisprosesSayisi;
            int y = Form1.arakatmanprosesSayisi;
            int z = Form1.cikisprosesSayisi;
            int iterasyon = Form1.iterasyonSayisi;
            double ogrenmeKatsayisi = 0.5;
            double momentumKatsayisi = 0.8;
            double toplam=0;

            int arakatmanAgirlikSayisi = x * y;
            int cikiskatmanAgirlikSayisi = y * z;
            double[] girisDegerleri = new double[x];
            double[] cikisDegerleri = new double[z];

            double[,] arakatmanagirlikDegerleri = new double[x,y];
            double[,] cikiskatmanagirlikDegerleri = new double[y,z];


            double[] arakatmanNetGirdi = new double[y];
            double[] arakatmanCikti = new double[y];
            double[] ciktikatmanNetGirdi = new double[z];
            double[] ciktikatmanCikti = new double[z];
            double[,] proseshataDegeri = new double[iterasyon,z];
            double[] sigmaDegeriCikis = new double[z];
            double[] sigmaDegeriAra = new double[y];


            double[,,] aaDegisimMiktari = new double[iterasyon, y, z];
            double[,,] aiDegisimMiktari = new double[iterasyon, x, y];


            int sayac,sayac1,sayac2;

            sayac = 0;
            foreach (TextBox textBox in girisProsesler)
            {
                
                double value = Convert.ToDouble(textBox.Text);
                girisDegerleri[sayac] = value;
                sayac= sayac + 1;
            }

            sayac1 = 0;
            sayac2 = 0;
            foreach (TextBox textBox in girisaraAgirliklar)
            {
                
                double value = Convert.ToDouble(textBox.Text);
                arakatmanagirlikDegerleri[sayac1,sayac2] = value;                
                sayac2++;
                if (sayac2 == y)
                {
                    sayac1++;
                    sayac2 = 0;
                }
                
            }



            sayac1 = 0;
            sayac2 = 0;
            foreach (TextBox textBox in aracikisAgirliklar)
            {

                double value = Convert.ToDouble(textBox.Text);
                cikiskatmanagirlikDegerleri[sayac1, sayac2] = value;
                sayac2++;
                if (sayac2 == z)
                {
                    sayac1++;
                    sayac2 = 0;
                }
            }

            sayac = 0;
            foreach (TextBox textBox in cikisProsesler)
            {

                double value = Convert.ToDouble(textBox.Text);
                cikisDegerleri[sayac] = value;
                sayac++;
            }


            foreach (var series in chart1.Series)
            {
                series.Points.Clear();
            }
            int dongu = 0;

            int c = 0;
            while (dongu<iterasyon)
            {
                //İLERİ HESAPLAMA
                //ara katman için net girdi                
                for (int j = 0; j < y; j++)
                {
                    for (int i = 0; i < x; i++)
                    {
                                                
                            arakatmanNetGirdi[j] = (girisDegerleri[i] * arakatmanagirlikDegerleri[i,j]) + arakatmanNetGirdi[j];
                            c=c+y;                        
                    }
                    c=c-y+1;
                }

                //ara katman için çıktı hesaplama
                for(int i=0; i<y ;i++)
                {
                    arakatmanCikti[i] = 1 / (1 + (1/Math.Exp(arakatmanNetGirdi[i])));

                }

                //çıktı katmanı için net girdi
                for (int m = 0; m < z; m++)
                {
                    for(int j=0;j<y;j++)
                    {
                        ciktikatmanNetGirdi[m] = (arakatmanCikti[j] * cikiskatmanagirlikDegerleri[j,m]) + ciktikatmanNetGirdi[m];

                    }
                }

                //çıktı katmanı için çıktı
                for (int m = 0; m < z; m++)
                {
                    ciktikatmanCikti[m] = 1 / (1 + (1 / Math.Exp(ciktikatmanNetGirdi[m])));

                    
                }

                //GERİ HESAPLAMA
                // Proses elemanlarının hata değerleri e=b-ç ve sigma degerleri
                for (int m=0; m<z ;m++)
                {
                    proseshataDegeri[dongu,m] = cikisDegerleri[m] - ciktikatmanCikti[m];
                    sigmaDegeriCikis[m] = ciktikatmanCikti[m] * (1 - ciktikatmanCikti[m]) * proseshataDegeri[dongu, m];
                    cikisDegerleri[m] = ciktikatmanCikti[m];
                }

                
                //Ara katman ile cıktı katman arasındaki ağırlık değiştirme Aa
                if (dongu==0)
                {
                    for (int j = 0; j < y; j++)
                    {
                        for (int m = 0; m < z; m++)
                        {
                            aaDegisimMiktari[dongu, j, m] = ogrenmeKatsayisi * sigmaDegeriCikis[m] * arakatmanCikti[j];
                        }
                    }

                    // Ara katman sigma degerleri
                    for (int j = 0; j < y; j++)
                    {
                        for (int m = 0; m < z; m++)
                        {
                            toplam = (sigmaDegeriCikis[m] * cikiskatmanagirlikDegerleri[j, m]) + toplam;
                            
                        }
                        sigmaDegeriAra[j] = arakatmanCikti[j] * (1 - arakatmanCikti[j]) * toplam;
                        toplam = 0;
                    }

                    //yeni ağırlık değerleri
                    for (int m = 0; m < z; m++)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            cikiskatmanagirlikDegerleri[j, m] = cikiskatmanagirlikDegerleri[j, m] + aaDegisimMiktari[dongu, j, m];
                        }
                    }
                }
                
                else
                {
                    for (int m = 0; m < z; m++)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            aaDegisimMiktari[dongu, j, m] = ogrenmeKatsayisi * sigmaDegeriCikis[m] * arakatmanCikti[j] + (momentumKatsayisi * aaDegisimMiktari[dongu - 1, j, m]);
                        }
                    }


                    // Ara katman sigma degerleri
                    for (int j = 0; j < y; j++)
                    {
                        for (int m = 0; m < z; m++)
                        {
                            toplam = sigmaDegeriCikis[m] * cikiskatmanagirlikDegerleri[j, m] + toplam;

                        }
                        sigmaDegeriAra[j] = arakatmanCikti[j] * (1 - arakatmanCikti[j]) * toplam;
                        toplam = 0;
                    }

                    //yeni ağırlık değerleri
                    for (int m = 0; m < z; m++)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            cikiskatmanagirlikDegerleri[j, m] = cikiskatmanagirlikDegerleri[j, m] + aaDegisimMiktari[dongu, j, m];
                        }
                    }
                }


                //Giriş katman ile ara katman arasındaki ağırlık değiştirme Ai
                if (dongu == 0)
                {
                    for (int j = 0; j < y; j++)
                    {
                        for (int i = 0; i < z; i++)
                        {
                            aiDegisimMiktari[dongu, i, j] = ogrenmeKatsayisi * sigmaDegeriAra[j] * girisDegerleri[i];
                        }
                    }

                    //yeni ağırlık değerleri
                    for (int j = 0; j < y; j++)
                    {
                        for (int i = 0; i < z; i++)
                        {
                            arakatmanagirlikDegerleri[i, j] = arakatmanagirlikDegerleri[i, j] + aiDegisimMiktari[dongu, i, j];
                        }
                    }
                }

                else
                {
                    for (int j = 0; j < y; j++)
                    {
                        for (int i = 0; i < z; i++)
                        {
                            aiDegisimMiktari[dongu, i, j] = ogrenmeKatsayisi * sigmaDegeriAra[j] * girisDegerleri[i] + momentumKatsayisi * aiDegisimMiktari[dongu-1, i, j];
                        }
                    }

                    //yeni ağırlık değerleri
                    for(int j = 0; j < y; j++)
                    {
                        for (int i = 0; i < z; i++)
                        {
                            arakatmanagirlikDegerleri[i, j] = arakatmanagirlikDegerleri[i, j] + aiDegisimMiktari[dongu - 1, i, j];
                        }
                    }
                }

                                             

                dongu++;
            }


            //EKRANA YAZDIRMA
            
            Label cikis = new Label();
            cikis.Location = new System.Drawing.Point(26, 300);
            cikis.Width = 150;
            cikis.Text = "Çıkış Değerleri";
            cikis.Font = new Font("Microsoft Sans Serif", 9.0f);
            this.Controls.Add(cikis);

            int konum = 0;
            for (int t = 0; t < z; t++)
                {

                    Label label = new Label();
                    label.Location = new System.Drawing.Point(26, 320 + (t + konum + 2));
                    label.Width = 150;
                    double a = Math.Round(cikisDegerleri[t], 3);
                    label.Text = (t+1) + ". Çıkış Değeri: " + a.ToString();
                    this.Controls.Add(label);

                    konum = konum + 25;
                }


            Label girisara = new Label();
            girisara.Location = new System.Drawing.Point(186, 300);
            girisara.Width = 150;
            girisara.Font = new Font("Microsoft Sans Serif", 9.0f);
            girisara.Text = "Giriş-Ara Ağırlık Değerleri";
            this.Controls.Add(girisara);

            konum = 0;
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {

                    Label label = new Label();
                    label.Location = new System.Drawing.Point(186, 320 + (i + konum + 2));
                    label.Width = 150;
                    double a = Math.Round(arakatmanagirlikDegerleri[i, j], 3);
                    label.Text = (i+1) + "." + (j+1) + ". : " + a.ToString();
                    this.Controls.Add(label);                    

                    konum = konum + 25;
                }

                konum = konum + 5;
            }


            Label aracikis = new Label();
            aracikis.Location = new System.Drawing.Point(346, 300);
            aracikis.Width = 150;
            aracikis.Font = new Font("Microsoft Sans Serif", 9.0f);
            aracikis.Text = "Ara-Çıkış Ağırlık Değerleri";
            
            this.Controls.Add(aracikis);

            konum = 0;
            for (int j = 0; j < y; j++)
            {
                for (int m = 0; m < z; m++)
                {

                    Label label = new Label();
                    label.Location = new System.Drawing.Point(346, 320 + (j + konum + 2));
                    label.Width = 150;
                    double a = Math.Round(cikiskatmanagirlikDegerleri[j, m],3);
                    label.Text = (j + 1) + "." + (m + 1) + ". : " + a.ToString();
                    this.Controls.Add(label);

                    konum = konum + 25;
                }

                konum = konum + 5;
            }

            Label proseshata = new Label();
            proseshata.Location = new System.Drawing.Point(506, 300);
            proseshata.Width = 150;
            proseshata.Font = new Font("Microsoft Sans Serif", 9.0f);
            proseshata.Text = "Hata Değerleri";

            this.Controls.Add(proseshata);

            konum = 0;
            for (int i = 0; i < iterasyon; i++)
            {
                for (int m = 0; m < z; m++)
                {

                    Label label = new Label();
                    label.Location = new System.Drawing.Point(506, 320 + (m + konum + 2));
                    label.Width = 150;
                    double a = Math.Round(proseshataDegeri[i, m], 3);
                    label.Text = (i + 1) + ". İterasyon" + (m + 1) + ".Proses : " + a.ToString();
                    this.Controls.Add(label);

                    konum = konum + 25;
                }

                konum = konum + 5;
            }


            //GRAFİĞE YAZDIRMA
            chart1.ChartAreas["ChartArea1"].AxisX.Title = "İterasyon";
            chart1.ChartAreas["ChartArea1"].AxisY.Interval = 0.05;
            chart1.ChartAreas["ChartArea1"].AxisY.Maximum = 0.4;
            chart1.ChartAreas["ChartArea1"].AxisY.Minimum = -0.4;
            for (int i = 0; i < iterasyon; i++)
            {
                double toplamHata = 0;
                for (int m = 0; m < z; m++)
                {                    
                    toplamHata = proseshataDegeri[i, m] + toplamHata;                                       
                }
                chart1.Series["HataDegerleri"].Points.Add(toplamHata);
                chart1.Series["HataDegerleri"].Points[i].AxisLabel = (i+1).ToString();
            }


        }
    }
}
